package apcl.sqtrinsp.main;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import apcl.sqtrinsp.R;
import apcl.sqtrinsp.CommonFunction;
import apcl.sqtrinsp.data.DBController;
import apcl.sqtrinsp.data.EssentialFileCopyController;
import apcl.sqtrinsp.data.dataclasses.SquatterRecord;
import apcl.sqtrinsp.insp.InspectionRecord;
import apcl.sqtrinsp.insp.ItemFragment;
import apcl.sqtrinsp.insp.dummy.DummyContent;
import apcl.sqtrinsp.insp.dummy.TestingItemFragment2;
import apcl.sqtrinsp.map.MapActivity;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ItemFragment.OnListFragmentInteractionListener {

    private final boolean debugDeleteDatabase = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener((View v) -> {
            DBController.saveSqtrRecord(new SquatterRecord(true));
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //navigation view in activity_main
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        switchFragments(R.id.nav_insp);

        checkPermission();




    }

    private void checkPermission(){
        if (CommonFunction.checkPermission(MainActivity.this)){
            //copy src database
            EssentialFileCopyController.fileCheck("", debugDeleteDatabase);
            //copy tpk (HAHQ for testing)
            EssentialFileCopyController.fileCheck("map", false);
            EssentialFileCopyController.fileCheck("map2", debugDeleteDatabase);
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.map) {
            CommonFunction.newIntent(getApplicationContext(), MapActivity.class);
            //Intent intent = new Intent(MainActivity.this, MapActivity.dataclasses);
            //startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switchFragments(item.getItemId());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void switchFragments(int id){
        Fragment fragment = null;
        switch (id){
            case R.id.nav_insp:
                fragment = new ItemFragment();
                break;
            case R.id.nav_others:
                fragment = new TestingItemFragment2();
                break;
        }

        if (fragment != null) {
            CommonFunction.switchFragment(this, R.id.main_fragment, fragment);
        }
    }

    @Override
    public void onListFragmentInteraction(SquatterRecord item) {
        Toast.makeText(this, "onListFragmentInteraction " + item.getSquatterid(), Toast.LENGTH_SHORT).show();
        Bundle bundle = new Bundle();
        bundle.putString("squatterid", item.getSquatterid());
        CommonFunction.newIntent(getApplicationContext(), InspectionRecord.class, bundle);
    }
}
