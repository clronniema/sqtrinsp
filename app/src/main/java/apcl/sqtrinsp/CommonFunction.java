package apcl.sqtrinsp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import apcl.sqtrinsp.SqtrInsp;
import apcl.sqtrinsp.main.MainActivity;

/**
 * Created by ronniema on 26-02-18.
 */

public class CommonFunction {

    private static final int REQUEST_WRITE_STORAGE = 112;

    public static Date getSystemDate(){
        return new Date();
    }

    public static String getSystemDateString(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return dateFormat.format(getSystemDate());
    }

    public static String dateConvertIntoString(Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return dateFormat.format(date);
    }

    public static Date stringConvertIntoDate(String dateStr) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return dateFormat.parse(dateStr);
    }

    public static void newIntent(Context c, Class className, Bundle extras){
        Intent intent = new Intent(c, className);
        if (extras !=null) {
            intent.putExtras(extras);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        c.startActivity(intent);
    }

    public static void newIntent(Context c, Class className){
        newIntent(c, className, null);
    }

    public static void switchFragment(AppCompatActivity activity, int currFragment, Fragment newFragment, Bundle args){
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        if (args != null) {
            newFragment.setArguments(args);
        }
        ft.replace(currFragment, newFragment);
        ft.commit();
    }
    public static void switchFragment(AppCompatActivity activity, int currFragment, Fragment newFragment){
        switchFragment(activity, currFragment, newFragment, null);
    }




    public static Boolean checkPermission(Activity activity){
        boolean hasPermission = (ContextCompat.checkSelfPermission(SqtrInsp.getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        boolean hasPermission3 = (ContextCompat.checkSelfPermission(SqtrInsp.getContext(),
                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
        boolean hasPermission4 = (ContextCompat.checkSelfPermission(SqtrInsp.getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
        boolean hasPermission5 = (ContextCompat.checkSelfPermission(SqtrInsp.getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission || !hasPermission3 || !hasPermission4 || !hasPermission5) {
            //if (!hasPermission || !hasPermission2 || !hasPermission3 || !hasPermission4 || !hasPermission5) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            //Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.CAMERA,  Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
                    }, REQUEST_WRITE_STORAGE);
            return false;
        }
        return true;
    }

}
