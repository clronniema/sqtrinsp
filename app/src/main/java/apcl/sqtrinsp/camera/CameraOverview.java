package apcl.sqtrinsp.camera;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;

import apcl.sqtrinsp.R;
import apcl.sqtrinsp.data.DBManager;
import apcl.sqtrinsp.data.FileObject;
import apcl.sqtrinsp.map.GridData;
import apcl.sqtrinsp.map.MapTransformation;

public class CameraOverview extends AppCompatActivity {

    CameraOverviewAdapter adapter;
    Boolean deleteEnabled = false;
    String squatterid;
    String photo_save_path;
    String function;
    Integer photo_counter;
    private static final int SELECT_PICTURE = 1;
    private static final int REQUEST_CAMERA_ACCESS = 113;
    private static final int REQUEST_LOCATION = 114;
    private static final int ACCESS_COARSE_LOCATION = 115;
    Location gps_xy;

    private static final int PICTURE_RESULT = 999;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_overview);
/*        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getGps();

        Intent intent = getIntent();
        if (intent != null) {
            squatterid = intent.getStringExtra("squatterid");
            photo_save_path = getFilesDir() + File.separator + getResources().getString(R.string.camera_path) + squatterid;
            File file = new File (photo_save_path);
            if (!file.isDirectory()){
                file.mkdirs();
            }

        }

        setupAdapter();




    }

    private void setupAdapter(){

        final ListView lv = (ListView) findViewById(R.id.lv_overview);
        adapter= new CameraOverviewAdapter(CameraOverview.this, getListOfPhotoPaths(), deleteEnabled, squatterid);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                //SHOW PHOTO AND IMG DESC

                /*                String file_name = lv.getAdapter().getItem(i).toString();

                File file = new File(file_name);
                boolean deleted = file.delete();
                notify();*/
                int position = i;
                Intent i2 = new Intent(CameraOverview.this, CameraViewer.class);
                i2.putExtra("squatterid", squatterid);
                i2.putExtra("position", position);
                startActivity(i2);
            }
        });
    }

    private ArrayList<String> getListOfPhotoPaths(){
        ArrayList<String> arrayList = new ArrayList<>();
        File directory = new File(photo_save_path);
        File[] files = directory.listFiles();
        for (int i = 0; i < files.length; i++)
        {
            if (files[i].toString().toLowerCase().endsWith(".jpg") || files[i].toString().toLowerCase().endsWith(".png"))
                arrayList.add(files[i].toString());
        }

        return arrayList;
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.cmm_camera_menu, menu);
            getMenuInflater().inflate(R.menu.camera_menu, menu);


        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.delete) {
            if (deleteEnabled){
                item.setTitle("刪除");
                /*if (function.equals("cmm")){
                    item.setTitle("刪除");
                } else {
                    item.setTitle("Delete");
                }*/

                deleteEnabled = false;
                setupAdapter();
            } else
            {
                item.setTitle("完成");/*
                if (function.equals("cmm")){
                    item.setTitle("完成");
                } else {
                    item.setTitle("Done");
                }*/
                deleteEnabled = true;
                setupAdapter();
            }
            return true;
        } else if (id == R.id.takepicture) {

            boolean hasPermission = (ContextCompat.checkSelfPermission(CameraOverview.this,
                    Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
            if (!hasPermission) {
                ActivityCompat.requestPermissions(CameraOverview.this,
                        new String[]{Manifest.permission.CAMERA},
                        REQUEST_CAMERA_ACCESS);
            }


            if (hasPermission) {
                boolean hasPermission2 = (ContextCompat.checkSelfPermission(CameraOverview.this,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
                if (!hasPermission2) {
                    ActivityCompat.requestPermissions(CameraOverview.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            REQUEST_LOCATION);
                }
                if (hasPermission2) {
                    if (check_gps()) {

                        AsyncTaskDownload dl = new AsyncTaskDownload();
                        dl.execute();

                    } else {
                        //Toast.makeText(this, "請你先開啟GPS，然後再使用相機。", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                Toast.makeText(this, "Insufficient permissions to access the camera, please enable permission to access location and camera.", Toast.LENGTH_SHORT).show();
            }
            return true;
        } else if (id == R.id.add) {
/*            Intent i = new Intent(CameraOverview.this, AddPhotos.class);
            startActivity(i);*/
            addPhotos();
            return true;
        } else if (id == android.R.id.home) {
            callFinish(RESULT_OK);
            return true;
        }
            return super.onOptionsItemSelected(item);
    }

    Boolean check_gps() {

        //return true;

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return true;
        } else {
            showGPSDisabledAlertToUser();
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case REQUEST_CAMERA_ACCESS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    //Intent intent = getIntent();
                    //finish();
                    //startActivity(intent);
                    //reload my activity with permission granted or use the features what required the permission
                } else
                {
                    ////Toast.makeText(getApplicationContext(), "The app was not allowed to access the camera. Hence, it cannot function properly. Please consider granting it this permission.", Toast.LENGTH_LONG).show();
                    //finish();
                }
            }
        }

    }

    private void addPhotos() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), 1);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICTURE_RESULT) { //
            if (resultCode == Activity.RESULT_OK) {
                ArrayList<Uri> uriArrList = new ArrayList<>();
                getClipDataFromCamera(uriArrList, data);
                new GetPhotos().execute(uriArrList);
            }
            updateGpsToDatabase();
        } else {
            if (resultCode == RESULT_OK) {
                if (requestCode == SELECT_PICTURE) {
                    ArrayList<Uri> uriArrList = new ArrayList<>();
                    getClipDataFromCamera(uriArrList, data);
                    new GetPhotos().execute(uriArrList);
                }
                updateGpsToDatabase();
            }
        }
    }

    private void getClipDataFromCamera(ArrayList<Uri> uriArrList, Intent data){
        if (data.getClipData() == null) {
            uriArrList.add(data.getData());
        } else {
            ClipData selectedImageUriCip = data.getClipData();
            int items = selectedImageUriCip.getItemCount();
            for (int i = 0; i < items; i++) {
                uriArrList.add(selectedImageUriCip.getItemAt(i).getUri());
            }
        }
    }

    private class GetPhotos extends AsyncTask<ArrayList<Uri>, Void, Void> {

        ProgressDialog dialog;
        protected void onPostExecute(Void dResult) {
            setupAdapter();
            dialog.cancel();

        }

        protected void onPreExecute() {

            dialog = new ProgressDialog(CameraOverview.this);
            dialog.setCancelable(true);
            dialog.setMessage("圖片處理中‧‧‧");
            dialog.show();

        }

        protected Void doInBackground(ArrayList<Uri>... params) {
            ArrayList<Uri> arrayList = params[0];
            // call upload photo here.
            for (Uri imageUri : arrayList) {
                //decodeFile(imageUri);
                Bitmap imageBitmap = getBitmap(imageUri);
                saveBitmap(imageBitmap);
            }
            return null;
        }

    }


    int get_last_photo_num(String folder_path) {
        int max_num = 1;
        ArrayList<Integer> file_list = new ArrayList();
        FileObject.getListInFolderNum(photo_save_path, file_list, false, "\\d+.jpg");
        Collections.sort(file_list);
        if (file_list.size() > 0) {
            try {
                /*Pattern p = Pattern.compile("\\d+");
                Matcher m = p.matcher(((File) file_list.get(file_list.size() - 1)).getName());
                if (m.find()) {*/
/*                    if (Integer.parseInt(m.group(0)) + 1 > max_num){
                        max_num = Integer.parseInt(m.group(0)) + 1;
                    }*/
                for (int i = 0; i < file_list.size(); i++){
                    if (file_list.get(i) >= max_num) {
                        max_num = file_list.get(i) + 1;
                    }
                }
                //}
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return max_num;
    }



    private Bitmap getBitmap(Uri uri) {
        ContentResolver mContentResolver = getContentResolver();
        InputStream in = null;
        try {
            final int IMAGE_MAX_SIZE = 150 * 1024;
            in = mContentResolver.openInputStream(uri);

            // Decode image size
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, options);
            in.close();


            int scale = 1;
            while ((options.outWidth * options.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }

            Bitmap resultBitmap = null;
            in = mContentResolver.openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                options = new BitmapFactory.Options();
                options.inSampleSize = scale;
                resultBitmap = BitmapFactory.decodeStream(in, null, options);

                // resize to desired dimensions
                int height = resultBitmap.getHeight();
                int width = resultBitmap.getWidth();

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(resultBitmap, (int) x,
                        (int) y, true);
                resultBitmap.recycle();
                resultBitmap = scaledBitmap;

                System.gc();
            } else {
                resultBitmap = BitmapFactory.decodeStream(in);
            }
            in.close();

            Log.d("photo", "bitmap size - width: " + resultBitmap.getWidth() + ", height: " +
                    resultBitmap.getHeight());
            return resultBitmap;
        } catch (IOException e) {
            Log.e("photo", e.getMessage(), e);
            return null;
        }
    }


    private void saveBitmap(Bitmap imageBitmap){
        ContentValues sysGen = new ContentValues();
        String file_path = "";
        Bitmap bitmap = imageBitmap;
        OutputStream os;
        if (photo_counter==null){
            photo_counter = 0;
        }
        try {
            File checkfile = new File(photo_save_path + File.separator + String.format("%06d", photo_counter) + ".jpg");
            if (checkfile.exists()) {
                photo_counter = get_last_photo_num(photo_save_path) + 1;
            }
            file_path = photo_save_path + File.separator + String.format("%06d", photo_counter) + ".jpg";
            os = new FileOutputStream(file_path);
            if (bitmap != null)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            sysGen.put("filepath", file_path);
            sysGen.put("squatterid", squatterid);

            DBManager dbManager = new DBManager(getApplicationContext());
            dbManager.insertWithoutKey("photorecord", sysGen);

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(CameraOverview.this, "Photo not saved. Please try again.", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onBackPressed(){

        // put your code here...
        callFinish(RESULT_OK);
        super.onBackPressed();
    }

    @Override
    public void onResume(){
        super.onResume();
        // put your code here...
        deleteEnabled = false;
        setupAdapter();

    }

    private void callFinish(int result_code){
        Intent returnIntent = new Intent();
        returnIntent.putExtra("photo_count", String.valueOf(getListOfPhotoPaths().size()));
        setResult(result_code, returnIntent);
        finish();
    }

    private class AsyncTaskDownload extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {



            resp = "";
            return resp;
        }


        @Override
        protected void onPostExecute(String result) {

            progressDialog.dismiss();
/*            Intent i = new Intent(CameraOverview.this, Camera2Activity.class);
            i.putExtra("function", function);
            i.putExtra("squatterid", squatterid);
            //i.putExtra("category", "flood_effect");
            startActivity(i);*/

            Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(camera, PICTURE_RESULT);
        }


        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(CameraOverview.this,
                    "Please wait",
                    "Photos are being saved"
            );
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                }
            }, 2000);
        }


        @Override
        protected void onProgressUpdate(String... text) {

        }


    }

    private void updateGpsToDatabase(){
        DBManager dbManager = new DBManager(getApplicationContext());

        if (gps_xy != null) {
            MapTransformation map_transform = new MapTransformation();
            GridData g_xy = map_transform.GEOHK(1, gps_xy.getLatitude(), gps_xy.getLongitude());

            dbManager.executeSQL("" +
                    "update noti_of_works " +
                    " set northing = " + String.valueOf(g_xy.getE()) + ", " +
                    " easting = " + String.valueOf(g_xy.getN()) + " " +
                    " where squatterid = '" + squatterid + "'");
        }
    }

    private void getGps(){
        gps_xy = null;

        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
        int res = getApplicationContext().checkCallingOrSelfPermission(permission);
        if ((res == PackageManager.PERMISSION_GRANTED)) {
            gps_xy = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 0,
                    new LocationListener() {
                        @Override
                        public void onLocationChanged(Location location) {
                            // TODO Auto-generated method stub

                            gps_xy = location;


                        }

                        @Override
                        public void onProviderDisabled(String provider) {
                            // TODO Auto-generated method stub
                        }

                        @Override
                        public void onProviderEnabled(String provider) {
                            // TODO Auto-generated method stub
                        }

                        @Override
                        public void onStatusChanged(String provider, int status,
                                                    Bundle extras) {
                            // TODO Auto-generated method stub
                        }
                    });
        }
    }


    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Go to Settings", (DialogInterface dialog, int id) ->
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //finish();
                        Intent callGPSSettingIntent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(callGPSSettingIntent);
                    }
                });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //finish();
                        dialog.cancel();
                    }
                });
        alertDialogBuilder.setMessage("GPS is disabled");
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


}

class CameraOverviewAdapter extends BaseAdapter {

    Activity activity;
    ArrayList<String> list;
    Boolean deleteEnabled;
    DBManager dbManager;
    String serial_no;
    public CameraOverviewAdapter(Activity activity, ArrayList<String> list, Boolean deleteEnabled, String serial_no){
        super();
        this.activity=activity;
        this.list=list;
        this.deleteEnabled=deleteEnabled;
        this.serial_no = serial_no;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }
    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return list.get(position);
    }
    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }



    public View getView(final int position, View view, final ViewGroup parent) {
        LayoutInflater inflater=activity.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.camera_overview_child, null,true);

        ImageView delete = (ImageView) rowView.findViewById(R.id.btn_delete);
        ImageView photo = (ImageView) rowView.findViewById(R.id.photo);
        dbManager = new DBManager((Context) activity);
        File imgFile = new  File(list.get(position));
        if(imgFile.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            photo.setImageBitmap(myBitmap);
        }
        if (deleteEnabled){
            delete.setVisibility(View.VISIBLE);
            delete.setEnabled(true);
        } else {
            delete.setVisibility(View.INVISIBLE);
            delete.setEnabled(false);
        }

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //delete
                File file = new File(list.get(position).toString());
                boolean deleted = file.delete();
                Log.d("delete photo", list.get(position).toString() + " " + String.valueOf(deleted));
                dbManager.executeSQL("delete from dsd_photo where photo_path = '" + list.get(position).toString() + "'");
                dbManager.executeSQL("delete from ins_photo_record where photo = '" + list.get(position).toString() + "'");
                list.remove(position);
                notifyDataSetInvalidated();

                if (list.size() == 0){
                    dbManager.executeSQL("update ins_flooding_situation set x = null, y = null where ins_ref_no = '" + serial_no + "'");
                }
            }
        });


        return rowView;

    };




}