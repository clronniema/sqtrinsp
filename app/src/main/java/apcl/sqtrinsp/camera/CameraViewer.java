package apcl.sqtrinsp.camera;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.EditText;
import android.widget.ImageView;


import java.io.File;
import java.util.ArrayList;

import apcl.sqtrinsp.R;
import apcl.sqtrinsp.data.DBManager;

public class CameraViewer extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private FragmentImage mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    String serial_number;
    int position;
    String photo_save_path;
    String function;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_viewer);

        Intent intent = getIntent();
        if (intent != null) {
            serial_number = intent.getStringExtra("squatterid");
            function = intent.getStringExtra("function");
            position = intent.getIntExtra("position", 1);
            photo_save_path = getFilesDir() + File.separator + getResources().getString(R.string.camera_path) + serial_number;
        }



/*        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.

        ArrayList<String> photos = new ArrayList<>();
        photos = getListOfPhotoPaths();

        ViewPager pager = (ViewPager) findViewById(R.id.container);
        FragmentManager fm = getSupportFragmentManager();
        FragmentImage pagerAdapter = new FragmentImage(fm, photos.size(), photos, function);
        // Here you would declare which page to visit on creation
        pager.setAdapter(pagerAdapter);
        pager.setCurrentItem(position);



    }

    private ArrayList<String> getListOfPhotoPaths(){
        ArrayList<String> arrayList = new ArrayList<>();
        File directory = new File(photo_save_path);
        File[] files = directory.listFiles();
        for (int i = 0; i < files.length; i++)
        {
            if (files[i].toString().toLowerCase().endsWith(".jpg") || files[i].toString().toLowerCase().endsWith(".png"))
                arrayList.add(files[i].toString());
        }

        return arrayList;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_camera_viewer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy(){

        super.onDestroy();
        System.gc();
    }


    public class FragmentImage extends FragmentStatePagerAdapter {
        int mNumOfTabs;
        Fragment fragment = null;
        ArrayList<String> photoPaths;
        String function;

        public FragmentImage(FragmentManager fm, int NumOfTabs, ArrayList<String> photoPaths, String function) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
            this.photoPaths = photoPaths;
            this.function = function;
        }

        @Override
        public Fragment getItem(int arg0) {
            for (int i = 0; i < mNumOfTabs ; i++) {
                if (i == position) {
                    Bundle bundle = new Bundle();
                    String photo_path = photoPaths.get(arg0);
                    bundle.putString("photo_path", photo_path);
                    bundle.putString("function", function);
                    fragment = new CameraFragmentControl();
                    fragment.setArguments(bundle);
                    break;
                }
            }
            return fragment;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return mNumOfTabs;
        }

    }

    public static class CameraFragmentControl extends Fragment {

        String photo_path;
        EditText et;
        String function;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            /** Getting the arguments to the Bundle object */


        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_camera_viewer,container, false);
            ImageView iv = (ImageView) view.findViewById(R.id.photo);
            et = (EditText) view.findViewById(R.id.photo_desc);

            Bundle data = getArguments();
            photo_path = data.getString("photo_path");
            function = data.getString("function");

            File imgFile = new  File(photo_path);
            if(imgFile.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                iv.setImageBitmap(myBitmap);
            }

            DBManager dbManager = new DBManager(getContext());

            String photo_desc = "";
            photo_desc = dbManager.getData("select description from photorecord where filepath = '" + photo_path + "'");
            et.setText(photo_desc);

            return view;
        }
        @Override
        public void onDestroyView(){
            super.onDestroyView();
            DBManager dbManager = new DBManager(getContext());
            dbManager.executeSQL("update photorecord set description = '" + et.getText().toString() + "' where filepath = '" + photo_path + "'");
        }

    }

}


