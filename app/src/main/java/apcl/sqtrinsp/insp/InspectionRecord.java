package apcl.sqtrinsp.insp;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import apcl.sqtrinsp.CommonFunction;
import apcl.sqtrinsp.R;
import apcl.sqtrinsp.camera.CameraActivity;
import apcl.sqtrinsp.camera.CameraOverview;
import apcl.sqtrinsp.data.DBController;
import apcl.sqtrinsp.data.dataclasses.SquatterRecord;
import apcl.sqtrinsp.map.MapActivity;

public class InspectionRecord extends AppCompatActivity {

    String squatterid;
    SquatterRecord squatterRecord;

    EditText etObjectid, etBookno, etFilename, etFileref, etHasremark, etHouseno, etIssue, etLocation, etPlanfileName, etSerialno, etSquatterid, etSurveyno, etSurveynoprefix, etRecorddate;
    EditText etDimensionBase, etDimensionHeight, etDimensionLength;
    Spinner spDlooffice, spDso, spScoffice, spStatus, spUsage, spMaterials;

    Toolbar toolbar;
    CollapsingToolbarLayout toolbar_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspection_record);
        toolbar_layout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            Bundle b = getIntent().getExtras();
            squatterid = b.getString("squatterid");
        } finally {
            if (squatterid==null){
                Toast.makeText(this, "Invalid squatter ID.", Toast.LENGTH_SHORT).show();
                finish();
            }
        }


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.camera);
        fab.setOnClickListener((View v) ->
                //Snackbar.make(v, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show()
                CommonFunction.newIntent(getApplicationContext(), CameraOverview.class)
        );

        initializeFields();
        loadData();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.map) {
            CommonFunction.newIntent(getApplicationContext(), MapActivity.class);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initializeFields(){
        spDlooffice = (Spinner) findViewById(R.id.dlooffice);
        spDso = (Spinner) findViewById(R.id.dso);
        spScoffice = (Spinner) findViewById(R.id.scoffice);
        spStatus = (Spinner) findViewById(R.id.status);
        spUsage = (Spinner) findViewById(R.id.usage);
        spMaterials = (Spinner) findViewById(R.id.materials);

        etDimensionBase = (EditText) findViewById(R.id.footdimension_b);
        etDimensionHeight = (EditText) findViewById(R.id.footdimension_h);
        etDimensionLength = (EditText) findViewById(R.id.footdimension_l);
        etObjectid = (EditText) findViewById(R.id.objectid);
        etBookno = (EditText) findViewById(R.id.bookno);
        etFilename = (EditText) findViewById(R.id.filename);
        etFileref = (EditText) findViewById(R.id.fileref);
        etHasremark = (EditText) findViewById(R.id.hasremark);
        etHouseno = (EditText) findViewById(R.id.houseno);
        etIssue = (EditText) findViewById(R.id.issue);
        etLocation = (EditText) findViewById(R.id.location);
        etPlanfileName = (EditText) findViewById(R.id.planfilename);
        etSerialno = (EditText) findViewById(R.id.serialno);
        etSquatterid = (EditText) findViewById(R.id.squatterid);
        etSurveyno = (EditText) findViewById(R.id.surveyno);
        etSurveynoprefix = (EditText) findViewById(R.id.surveynoprefix);
        etRecorddate = (EditText) findViewById(R.id.recorddate);
    }

    private void loadData(){
        squatterRecord = DBController.loadSqtrRecord(squatterid);

        spDlooffice.setSelection(squatterRecord.getDlooffice());
        spDso.setSelection(squatterRecord.getDso());
        spScoffice.setSelection(squatterRecord.getScoffice());
        spStatus.setSelection(squatterRecord.getStatus());
        spUsage.setSelection(squatterRecord.getUsage());
        spMaterials.setSelection(squatterRecord.getMaterials());

        etDimensionBase.setText(squatterRecord.getFootdimension_b().toString());
        etDimensionHeight.setText(squatterRecord.getFootdimension_h().toString());
        etDimensionLength.setText(squatterRecord.getFootdimension_l().toString());

        etObjectid.setText(squatterRecord.getObjectid().toString());
        etBookno.setText(squatterRecord.getBookno());
        etFilename.setText(squatterRecord.getFilename());
        etFileref.setText(squatterRecord.getFileref());
        etHasremark.setText(squatterRecord.getHasremark());
        etHouseno.setText(squatterRecord.getHouseno());
        etIssue.setText(squatterRecord.getIssue());

        etLocation.setText(squatterRecord.getLocation());
        etPlanfileName.setText(squatterRecord.getPlanfilename());
        etSerialno.setText(squatterRecord.getSerialno());
        etSquatterid.setText(squatterRecord.getSquatterid());
        etSurveyno.setText(squatterRecord.getSurveyno());
        etSurveynoprefix.setText(squatterRecord.getSurveynoprefix());
        etRecorddate.setText(squatterRecord.getRecorddateStr());

        toolbar_layout.setTitle(squatterid);


    }
}
