package apcl.sqtrinsp.map;

public class GridData
{
	public double n;
	public double e;

	public GridData()
	{
		n = 0;
		e = 0;
	}

	public double getN()
	{	return n;		}

	public double getE()
	{	return e;		}

	void setN(double input)
	{	n = input;		}

	void setE(double input)
	{	e = input;		}
}
