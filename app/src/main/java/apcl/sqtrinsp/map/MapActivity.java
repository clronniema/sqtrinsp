package apcl.sqtrinsp.map;


import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.esri.android.map.FeatureLayer;
import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.LocationDisplayManager;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISLocalTiledLayer;
import com.esri.android.map.event.OnLongPressListener;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.android.map.event.OnZoomListener;
import com.esri.android.runtime.ArcGISRuntime;
import com.esri.android.toolkit.map.MapViewHelper;
import com.esri.core.geodatabase.Geodatabase;
import com.esri.core.geodatabase.GeodatabaseFeatureTable;
import com.esri.core.geometry.Geometry;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.Polygon;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.CallbackListener;
import com.esri.core.map.Feature;
import com.esri.core.map.FeatureResult;
import com.esri.core.map.Graphic;
import com.esri.core.symbol.PictureMarkerSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol;
import com.esri.core.symbol.TextSymbol;
import com.esri.core.tasks.query.QueryParameters;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import apcl.sqtrinsp.R;
import apcl.sqtrinsp.data.DBController;
import apcl.sqtrinsp.data.dataclasses.NewSqtrLoc;

public class MapActivity extends AppCompatActivity {

	ProgressDialog progress;


	MapView map;
	MapViewHelper mvHelper;
	private boolean m_isMapLoaded;
	//ArcGISFeatureLayer featureLayer;

	// ArcGISTiledMapServiceLayer tileLayer;
	private ArcGISLocalTiledLayer localTileLayer;

	private GeodatabaseFeatureTable gdbFeatureTable;
	private Geodatabase gdb_tree;
	private FeatureLayer featureLayer;

	private String tpk_path;
	private String gdb_tree_file_path;
	private String plan_est_num = "";
	private String SQUATTERID = "";
	private String accessRights = "";
	private String whereClause;
	private String tree_prfx_code;


	private double previous_scale = 5000;
	private int zoom_level = 5;


	private Button btnExtent;
	private TextView tvScale;

	private Geometry geometryTree;

	private GraphicsLayer myGraphicalLayer;
	MapView mMapView;
	ArcGISLocalTiledLayer baseLayer;
	private LocationManager mlocManager;
	private LocationListener mlocListener;
	GraphicsLayer graphicsLayer;
	GraphicsLayer labelLayer;

	GraphicsLayer addSquatterLocationLayer;
	Graphic newLocationGraphic1;

	Boolean canAddLocation = true;
	// 0 = none, 1 = done, 2 = moving;
	int addTreeMode = 0;
	ImageView newTreeLocation;
	MenuItem addTreeMenuItem;
	MenuItem removeTreeMenuItem;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//ArcGISRuntime.setClientId("yEE2e62kDLRzlHy5");
/*		setContentView(R.layout.dummy);
		Toast.makeText(MapActivity.this, "Map temporarily disabled.", Toast.LENGTH_SHORT).show();
		finish();*/
		setContentView(R.layout.activity_map);
		map = (MapView) findViewById(R.id.map);
		//getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		getSupportActionBar().show();

		tree_prfx_code = "HAHQ";

		TextView disclaimer = (TextView) findViewById(R.id.disclaimer);
		disclaimer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder1 = new AlertDialog.Builder(MapActivity.this);
				builder1.setTitle("Disclaimer Notice:");
				builder1.setMessage("The map information provided on this web site is protected by copyright owned by " +
						"the Government of the Hong Kong Special Administrative Region (the " +
						"“Government”). No express or implied warranty is given to the accuracy or " +
						"completeness of such map information or its appropriateness for use in any " +
						"particular circumstances. The Government is not responsible for any loss or damage " +
						"whatsoever arising from any cause in connection with such map information or with " +
						"this web site.");
				builder1.setCancelable(true);
				builder1.setNeutralButton(
						"Accept",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
				AlertDialog alert11 = builder1.create();
				alert11.show();
			}
		});


		Intent intent = getIntent();
		String str_err = "";
		if (intent != null) {
			if (intent.getStringExtra("tree_prfx_code") != null && !intent.getStringExtra("tree_prfx_code").equals("")) {
				//photo_save_path = photo_save_path + File.separator + intent.getStringExtra("type") + File.separator + intent.getStringExtra("form_id");
				tree_prfx_code = intent.getStringExtra("tree_prfx_code");
				tpk_path = getFilesDir() + File.separator + getResources().getString(R.string.map_path) + File.separator + intent.getStringExtra("tree_prfx_code").replaceAll("/", "_") + ".tpk";
				//tpk_path = Environment.getExternalStorageDirectory() + File.separator + getResources().getString(R.string.map_path) + File.separator + intent.getStringExtra("tree_prfx_code").replaceAll("/", "_") + ".tpk";

			}

			if (intent.getStringExtra("SQUATTERID") != null && !intent.getStringExtra("SQUATTERID").equals("")) {
				//photo_save_path = photo_save_path + File.separator + intent.getStringExtra("type") + File.separator + intent.getStringExtra("form_id");
				//intent.putExtra("prvl", accessRights);
				accessRights = intent.getStringExtra("prvl") == null ? "" : intent.getStringExtra("prvl");
				SQUATTERID = intent.getStringExtra("SQUATTERID");
				//setTitle(getTitle() + " (" + SQUATTERID + " )");
			}
		}

/*
		if (accessRights.contains("SAVE") && !accessRights.contains("AUDIT")) {

		} else {
			canAddLocation = false;
		}
*/


		//debug
        //SQUATTERID  = "HAHQ-T0039";
		tpk_path = getFilesDir() + getResources().getString(R.string.map_path) + File.separator + tree_prfx_code.replaceAll("/", "_") + ".tpk";
		gdb_tree_file_path = getFilesDir() + getResources().getString(R.string.map_path) + "/tree.geodatabase";

		try {
			File file_tpk = new File(tpk_path);
			File file_gdb = new File(gdb_tree_file_path);

			mvHelper = new MapViewHelper(map);

			if (!file_tpk.exists())
				str_err += "Estate map file not found. ";
			if (!file_gdb.exists())
				str_err += "Tree data file not found. ";
			if (tree_prfx_code.equals(""))
				str_err += "Estate information not found. ";

			//if (map != null) {
			if (str_err.equals("")) {
				try {
					//add estate tpk
					//localTileLayer = new ArcGISLocalTiledLayer("file:///" + Environment.getExternalStorageDirectory() + File.separator + getResources().getString(R.string.map_path) + "/ETrMS_demo.tpk");
					localTileLayer = new ArcGISLocalTiledLayer("file:///" + tpk_path);
					localTileLayer.setVisible(true);
					map.addLayer(localTileLayer);


					// gdb_tree_file_path = null;
					try {
						//localGdb = new Geodatabase(Environment.getExternalStorageDirectory() + File.separator + getResources().getString(R.string.map_path) + "/tree.geodatabase");
						//gdb_tree  = new Geodatabase(Environment.getExternalStorageDirectory() + File.separator + getResources().getString(R.string.map_path) + "/tree.geodatabase");
						gdb_tree = new Geodatabase(gdb_tree_file_path);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}

					//add tree layer
					gdbFeatureTable = gdb_tree.getGeodatabaseFeatureTableByLayerId(0);
					featureLayer = new FeatureLayer(gdbFeatureTable);
					//filter by tree_prfx_code
					featureLayer.setDefinitionExpression("TREE_ID like '" + tree_prfx_code + "-%' ");
					//featureLayer.setMinScale(2000);
					featureLayer.setEnableLabels(true);
					featureLayer.setSelectionColor(Color.RED);
					featureLayer.setSelectionColorWidth(10);
					featureLayer.setMinScale(2000);
					map.addLayer(featureLayer);

/*					map.setOnStatusChangedListener(new OnStatusChangedListener() {
						public void onStatusChanged(Object source, STATUS status) {
							if (source == featureLayer && status == STATUS.LAYER_LOADED) {

								whereClause = "SQUATTERID='" + SQUATTERID + "'";
								QueryParameters queryParams = new QueryParameters();
								queryParams.setWhere(whereClause);
								featureLayer.clearSelection();
								featureLayer.selectFeatures(queryParams, FeatureLayer.SelectionMode.NEW, new CallbackListener<FeatureResult>()
										{
											@Override
											public void onError(Throwable ex) {
												// Highlight errors to the user.
												// showToast("Error querying FeatureServiceTable");
												System.out.print("Error featureLayer");
											}
											@Override
											public void onCallback(FeatureResult objs) {
												if (objs.featureCount() == 0)
													return;
												for (Object objFeature : objs) {
													Feature feature = (Feature) objFeature;
													map.centerAt((Point)feature.getGeometry(), false);
													map.setScale(500, false);
													//featureLayer.selectFeature(feature.getId());
												}
											}
										}
								);
							}
						}
					});*/

					map.setOnStatusChangedListener(new OnStatusChangedListener() {
						public void onStatusChanged(Object source, STATUS status) {
							if (source == featureLayer && status == STATUS.LAYER_LOADED) {

								//zoom to tree if SQUATTERID provide else zoom the estate's extent

								//whereClause = "SQUATTERID = '" + SQUATTERID  + "'";
								whereClause = "TREE_ID like '" + tree_prfx_code + "-%' ";
								// Create query parameters, based on the constructed where clause.
								QueryParameters queryParams = new QueryParameters();
								queryParams.setWhere(whereClause);

								// Execute the query and create a callback for dealing with the results of the query.
								gdbFeatureTable.queryFeatures(queryParams, new CallbackListener<FeatureResult>() {

									@Override
									public void onError(Throwable ex) {
										// Highlight errors to the user.
										// showToast("Error querying FeatureServiceTable");
										System.out.print("Error gdbFeatureTable");
									}

									@Override
									public void onCallback(FeatureResult objs) {
										//exit when result is not one
										if (objs.featureCount() == 0)
											return;

										// If there are no query results, inform user.
										//if (objs.featureCount() < 1) {
										// showToast("No results");
										//    return;
										//}
										// Iterate the results and select each feature.

										labelLayer = new GraphicsLayer();
										labelLayer.setMinScale(500);


										for (Object objFeature : objs) {
											Feature feature = (Feature) objFeature;
											if (feature.getAttributes().get("TREE_ID").toString().equals(SQUATTERID)) {
												Point graphicPoint = (Point) feature.getGeometry();
												geometryTree = feature.getGeometry();
												SimpleMarkerSymbol sym = new SimpleMarkerSymbol(Color.RED, 12, SimpleMarkerSymbol.STYLE.CIRCLE);
												map.centerAt(graphicPoint, false);
												map.setScale(500, false);
												GraphicsLayer graphicsLayer = new GraphicsLayer();
												map.addLayer(graphicsLayer);
												Graphic graphic1 = new Graphic(graphicPoint, sym);
												graphicsLayer.addGraphic(graphic1);
											}
											Point graphicPoint = (Point) feature.getGeometry();
											String displayLabel = feature.getAttributes().get("TREE_ID").toString().substring(feature.getAttributes().get("TREE_ID").toString().indexOf("-T") + 2, feature.getAttributes().get("TREE_ID").toString().length());
											TextSymbol ts = new TextSymbol(14, displayLabel, getResources().getColor(R.color.colorPrimary));
											ts.setOffsetY(20);
											Graphic tsPoint = new Graphic(graphicPoint, ts);
											labelLayer.addGraphic(tsPoint);
										}

										map.addLayer(labelLayer);
										//map.addLayer(labelLayer);
									}
								});
							}

						}
					});

					/*whereClause = "SQUATTERID like '" + tree_prfx_code + "-%' ";
					// Create query parameters, based on the constructed where clause.
					QueryParameters queryParams = new QueryParameters();
					queryParams.setWhere(whereClause);

					// Execute the query and create a callback for dealing with the results of the query.
					gdbFeatureTable.queryFeatures(queryParams, new CallbackListener<FeatureResult>()
					{

						@Override
						public void onError(Throwable ex) {
							// Highlight errors to the user.
							// showToast("Error querying FeatureServiceTable");
							System.out.print("Error gdbFeatureTable");
						}

						@Override
						public void onCallback(FeatureResult objs) {
							//exit when result is not one
							if (objs.featureCount() != 1)
								return;

							labelLayer = new GraphicsLayer();
							labelLayer.setMinScale(500);

							for (Object objFeature : objs) {
								Feature feature = (Feature) objFeature;
								Point graphicPoint =(Point)feature.getGeometry();
								String displayLabel = feature.getAttributes().get("TREE_ID").toString().substring(feature.getAttributes().get("TREE_ID").toString().indexOf("-T"), feature.getAttributes().get("TREE_ID").toString().length());
								TextSymbol ts = new TextSymbol(12, displayLabel, getResources().getColor(R.color.black2));
								ts.setOffsetY(20);
								Graphic tsPoint = new Graphic(graphicPoint, ts);
								labelLayer.addGraphic(tsPoint);
							}
							map.addLayer(labelLayer);
						}
					});
*/


					//addListener();
					// map.addLayer(featureLayer);

				} catch (Exception e) {
					e.printStackTrace();
				} finally {

				}


			} else {
				//Toast.makeText(getApplicationContext(), "Map file not find",
				//        Toast.LENGTH_LONG).show();

				AlertDialog.Builder builder = new AlertDialog.Builder(MapActivity.this);
				builder.setTitle(str_err);
				builder.setItems(new CharSequence[]
								{"OK"},
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								switch (which) {
									case 0:
										finish();
										break;
								}
							}
						});
				builder.create().show();

			}

		} catch (Exception e) {

			Toast.makeText(getApplicationContext(), e.getMessage(),
					Toast.LENGTH_LONG).show();
		}


/*		mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		mlocListener = new MyLocationListener();
		mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, mlocListener);
		mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, mlocListener);
// loading the map
*//*		mMapView = map;
		baseLayer = localTileLayer;
		mMapView.addLayer(baseLayer);*//*
// defining my position layer
		myGraphicalLayer = new GraphicsLayer();*/

		//int draw = mvHelper.addMarkerGraphic(map.getCenter().getY(), map.getCenter().getX(), "tree_loc", "snippet", 1, getApplicationContext().getResources().getDrawable(R.drawable.about), true, 0);

		Log.d("test", "test");

		if (canAddLocation) {
			map.setOnLongPressListener(new OnLongPressListener() {
				@Override
				public boolean onLongPress(float v, float v1) {
/*				Toast.makeText(MapActivity.this, "long press", Toast.LENGTH_SHORT).show();
				int[] selected = addSquatterLocationLayer.getGraphicIDs(v, v1, 10, 1);
				if (selected.length > 0) {
					Log.d("tag", String.valueOf(selected[0]));

				}*/
					Point p = map.toMapPoint(v, v1);
					//Point p = (Point) addSquatterLocationLayer.getGraphic(selected[0]).getGeometry();
					addTreeLocation(p);
					return true;
				}
			});
		}
/*		MapOnTouchListener listener = new MapOnTouchListener(getApplicationContext(), map);
		map.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				//final GeoPoint pt = mv.getMapView().getMapCenter();
				//doing my stuff here
				Toast.makeText(MapActivity.this, "short", Toast.LENGTH_SHORT).show();
				return true; //ignored anyway
			}
		});*/

		newTreeLocation = (ImageView) findViewById(R.id.newTreeLocationMarker);
		if (canAddLocation) {
			// 0 = none, 1 = done, 2 = moving, -1 = cannot;
			//no point, show "ADD"
			changeLocationEditMode(0);
		} else if (1 == 2) {
			//is done, show "MOVE"
			changeLocationEditMode(1);
		} else if (1 == 3) {
			//is moving, show "DONE"
			changeLocationEditMode(2);
		} else {
			changeLocationEditMode(-1);
		}
	}


	private boolean isLocationEnabled(Context context) {
		int locationMode = 0;
		String locationProviders;

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			try {
				locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

			} catch (Settings.SettingNotFoundException e) {
				e.printStackTrace();
			}

			return locationMode != Settings.Secure.LOCATION_MODE_OFF;

		} else {
			locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
			return !TextUtils.isEmpty(locationProviders);
		}
	}

	void addListener() {
		//zoom to estate or tree
		btnExtent = (Button) findViewById(R.id.btnExtent);
		/*tvScale  = (TextView)findViewById(R.id.tvScale);
		tvScale.setText("");
		if (SQUATTERID.equals("")){
			btnExtent.setText("Back to estate ");
		}else {

			btnExtent.setText("Back to tree location ");
		}*/

		btnExtent.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//zoom to tree point
				if (geometryTree != null && map != null) {
					Point graphicPoint = (Point) geometryTree;
					map.centerAt(graphicPoint, false);
					map.setScale(500, false);
				} else if (map != null) {

					if (map.getLayer(0) != null) {
						map.setExtent(map.getLayer(0).getFullExtent());
					}
					//zoom to estate full extent
				}
			}
		});

		if (map != null) {
			map.setOnZoomListener(new OnZoomListener() {
					/*
					100
					500
					1000
					2000
					5000
					 */

				@Override
				public void postAction(float arg0, float arg1, double arg2) {
					// TODO Auto-generated method stub

					try {

						String scale = "";
						double current_scale = map.getScale();
						if (current_scale > 5000) {
							scale = "5000";
							//map.setScale(5000);
						} else if (current_scale > 2000 && current_scale <= 5000) {
							scale = "2000";
							//map.setScale(2000);
						} else if (current_scale > 1000 && current_scale <= 2000) {
							scale = "1000";
							//map.setScale(1000);
						} else if (current_scale > 500 && current_scale <= 1000) {
							scale = "500";
							//map.setScale(500);
						} else if (current_scale > 100 && current_scale <= 500) {
							scale = "100";
							//map.setScale(100);

						}
						//tvScale.setText("Current scale:" + Double.toString(Math.round(map.getScale())));
						//tvScale.setText(" Scale - 1:" + scale);

					} catch (Exception e) {


					}


				}

				@Override
				public void preAction(float arg0, float arg1, double arg2) {
					// TODO Auto-generated method stub

				}
			});


			if (canAddLocation) {
				map.setOnLongPressListener(new OnLongPressListener() {

					private static final long serialVersionUID = 1L;

					@Override

					public boolean onLongPress(float x, float y) {
						Point mapPoint = map.toMapPoint(x, y);
						GraphicsLayer graphicsLayer = new GraphicsLayer();
						map.zoomTo(mapPoint, 1);
						map.addLayer(graphicsLayer);
						Graphic graphic1 = new Graphic(map.toMapPoint(new Point(x, y)), new SimpleMarkerSymbol(Color.BLUE, 12, SimpleMarkerSymbol.STYLE.CIRCLE));

						graphicsLayer.addGraphic(graphic1);
						//Toast.makeText(getApplicationContext(), "Long press x=" + mapPoint.getX() + ",y=" + mapPoint.getY(), Toast.LENGTH_LONG).show();
						Toast.makeText(getApplicationContext(), "Added to map.", Toast.LENGTH_LONG).show();
						//Toast.makeText(getApplicationContext(), String.valueOf(map.getScale()), Toast.LENGTH_LONG).show();
						return true;
					}
				});
			}

		}
	}

	void setMapZoom(int zoom) {
		/*
			100
			500
			1000
			2000
			5000
		 */

		double scale;
		scale = -1;
		if (this.map != null) {
			switch (zoom) {

				case 1:
					scale = 100;
					break;
				case 2:
					scale = 500;
					break;
				case 3:
					scale = 1000;
					break;
				case 4:
					scale = 2000;
					break;
				case 5:
					scale = 5000;
					break;
			}
			this.map.setScale(scale);
		}

	}

	private void getLocator() {
		Polygon mapExtent = map.getExtent();


		LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			// TODO: Consider calling
			//    ActivityCompat#requestPermissions
			// here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation
			// for ActivityCompat#requestPermissions for more details.
			return;
		}
		lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, new LocationListener() {
			@Override
			public void onLocationChanged(Location location) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onStatusChanged(String provider, int status,
										Bundle extras) {
				// TODO Auto-generated method stub
			}
		});


		//Criteria crit = new Criteria();
		//String towers = lm.getBestProvider(crit, false);
		Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);


		if(location != null)
		{

			MapTransformation transform = new MapTransformation();

			GridData data = transform.GEOHK(2, location.getLatitude(), location.getLongitude());

			Point point = new Point();

			point.setXY(data.e, data.n);

			//Point point = GeometryEngine.project(data.e, data.n, mMapView.getSpatialReference());

			map.centerAt(point, false);
			//map.setScale(5000, false);

			graphicsLayer = new GraphicsLayer();


			//mMapView.zoomTo(mapPoint, 1);


			map.addLayer(graphicsLayer);

			Drawable d = getResources().getDrawable(android.R.drawable.ic_input_add);
			PictureMarkerSymbol sym = new PictureMarkerSymbol(d);
			sym.setOffsetY(sym.getHeight()/2);

			//Graphic graphic1 = new Graphic(point,sym);
			//Graphic graphic1 = new Graphic(point,new SimpleMarkerSymbol(Color.RED,10, SimpleMarkerSymbol.STYLE.CIRCLE));
			Graphic graphic1 = new Graphic(point,new SimpleMarkerSymbol(Color.RED,20, SimpleMarkerSymbol.STYLE.X));
			//graphic.setGeometry();
			graphicsLayer.addGraphic(graphic1);

			//Toast.makeText(getApplicationContext(),"GPS PROVIDER", Toast.LENGTH_LONG).show();
		}
		else
		{
			try {
				lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, new LocationListener() {
					@Override
					public void onLocationChanged(Location location) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onProviderDisabled(String provider) {
						// TODO Auto-generated method stub
					}

					@Override
					public void onProviderEnabled(String provider) {
						// TODO Auto-generated method stub
					}

					@Override
					public void onStatusChanged(String provider, int status,
												Bundle extras) {
						// TODO Auto-generated method stub
					}
				});
				Location location1 = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

				if (location1 != null) {
					MapTransformation transform = new MapTransformation();
					GridData data = transform.GEOHK(2, location1.getLatitude(), location1.getLongitude());
					Point point = new Point();
					point.setXY(data.e, data.n);

					//Point point = GeometryEngine.project(data.e, data.n, mMapView.getSpatialReference());

					map.centerAt(point, false);
					//map.setScale(5000, false);
					graphicsLayer = new GraphicsLayer();

					map.addLayer(graphicsLayer);

					Drawable d = getResources().getDrawable(android.R.drawable.ic_input_add);
					PictureMarkerSymbol sym = new PictureMarkerSymbol(d);
					//Graphic graphic1 = new Graphic(point,sym);
					//Graphic graphic1 = new Graphic(point,new SimpleMarkerSymbol(Color.RED,10, SimpleMarkerSymbol.STYLE.CIRCLE));
					Graphic graphic1 = new Graphic(point, new SimpleMarkerSymbol(Color.RED, 20, SimpleMarkerSymbol.STYLE.X));
					//Graphic graphic1 = new Graphic(point,new SimpleMarkerSymbol(Color.RED,25,STYLE.CIRCLE));
					//graphic.setGeometry();
					graphicsLayer.addGraphic(graphic1);

					//Toast.makeText(getApplicationContext(),"NETWORK PROVIDER", Toast.LENGTH_LONG).show();
				}
			} catch (SecurityException e){
				Snackbar.make(mMapView, "Security Exception.", Snackbar.LENGTH_LONG);
			}
		}
	}

	private void startLocator(){
		map.getLocationDisplayManager().start();
		map.getLocationDisplayManager().setAutoPanMode(LocationDisplayManager.AutoPanMode.LOCATION);
		//map.getLocationDisplayManager().setAutoPanMode(LocationDisplayManager.AutoPanMode.NAVIGATION);
	}

	private void stopLocator(){
		map.getLocationDisplayManager().stop();
	}

	private void fixLocator(){
		LocationDisplayManager.AutoPanMode autoPanMode = LocationDisplayManager.AutoPanMode.OFF;
		map.getLocationDisplayManager().setAutoPanMode(autoPanMode);
	}

	private void SetMyLocationPoint(final double x, final double y) {
		PictureMarkerSymbol myPin = new PictureMarkerSymbol(getResources().getDrawable(
				android.R.drawable.ic_input_add));

		Point wgspoint = new Point(x, y);
		Point mapPoint = (Point) GeometryEngine.project(wgspoint, SpatialReference.create(4326),
				map.getSpatialReference());

		Graphic myPinGraphic = new Graphic(mapPoint, myPin);

		try {
			myGraphicalLayer.removeAll();
		} catch (Exception e) {
			e.printStackTrace();
		}

		myGraphicalLayer.addGraphic(myPinGraphic);
		myGraphicalLayer.setVisible(true);
		map.addLayer(myGraphicalLayer);

	}
	public class MyLocationListener implements LocationListener {

		@Override
		public void onLocationChanged(Location loc) {
			MapTransformation map_transform = new MapTransformation();
			GridData g_xy;
			g_xy = map_transform.GEOHK(2
					, loc.getLatitude()
					, loc.getLongitude());
			SetMyLocationPoint(g_xy.getE(), g_xy.getN());
		}

		@Override
		public void onProviderDisabled(String provider) {
			Toast.makeText(getApplicationContext(), "provider enabled", Toast.LENGTH_SHORT)
					.show();
		}

		@Override
		public void onProviderEnabled(String provider) {
			Toast.makeText(getApplicationContext(), "provider disabled", Toast.LENGTH_SHORT)
					.show();
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	private void saveLocation(){
		Point p = getCurrentLocationAsTreeLocation();
		if (!SQUATTERID.equals("") && p != null) {
			Calendar c = Calendar.getInstance();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			String formattedDate = df.format(c.getTime());

			NewSqtrLoc newSqtrLoc = new NewSqtrLoc(
					-1,
					"HAHQ-T0001",
					"None",
					p.getX(),
					p.getY()
			);
			DBController.saveNewSqtrLoc(newSqtrLoc);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
/*		if (canAddLocation && accessRights.contains("SAVE") && !accessRights.contains("AUDIT")){
			inflater.inflate(R.menu.in_map_menu_final, menu);
			addTreeMenuItem = (MenuItem) findViewById(R.id.add_location);
			removeTreeMenuItem = (MenuItem) findViewById(R.id.remove_location);
			loadTreeLocation();
		} else {
			inflater.inflate(R.menu.in_map_menu2, menu);
		}*/
		inflater.inflate(R.menu.in_map_menu_final, menu);
		addTreeMenuItem = (MenuItem) findViewById(R.id.add_location);
		removeTreeMenuItem = (MenuItem) findViewById(R.id.remove_location);
		loadTreeLocation();
		return super.onCreateOptionsMenu(menu);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
/*			case R.id.to_main_menu:
				intent = new Intent(this,MainActivity.dataclasses);
				intent.putExtra("UserLgnNameStr", mSingleton.getUSER_ID());
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				return true;*/
			case android.R.id.home:
				finish();
				return true;
			case R.id.location:
				if (isLocationEnabled(getApplicationContext())) {
					if (map.getLocationDisplayManager().isStarted()){
						stopLocator();
						item.setIcon(getResources().getDrawable(R.drawable.ic_edit_location_white_48dp));
					} else {
						startLocator();
						item.setIcon(getResources().getDrawable(R.drawable.ic_my_location_white_48dp));
					}
				} else {
					showGPSDisabledAlertToUser();
				}
				break;
			case R.id.add_location:
				// 0 = none, 1 = done, 2 = moving, -1 = cannot;
/*				if (getLocationEditMode() == 0) {
					addTreeLocation();
				} else if (getLocationEditMode() == 1){
					addTreeLocation();
				} else if (getLocationEditMode() == 2){
					clearTreeLocation();
				} else {

				}*/
				addTreeLocation(null);
				break;
			case R.id.remove_location:
				removeTreeLocation();
				break;
		}

		return false;
	}

	private void changeLocationEditMode(int newMode){
		// 0 = none, 1 = done, 2 = moving, -1 = cannot;
/*
		addTreeMode = newMode;
		if (newMode == -1){
		} else if (newMode == 0 ){
			addTreeMenuItem.setTitle("Add");
		} else if (newMode == 1 ){
			addTreeMenuItem.setTitle("Move");
		} else {
			addTreeMenuItem.setTitle("Done");
		}
*/

	}

	private int getLocationEditMode(){
		// 0 = none, 1 = done, 2 = moving, -1 = cannot;
		return addTreeMode;
	}


	private void showGPSDisabledAlertToUser() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setMessage("Turn on location services to allow \"ETrMS\" to determine your location")
				.setCancelable(false)
				.setPositiveButton("Settings",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Intent callGPSSettingIntent = new Intent(
										Settings.ACTION_LOCATION_SOURCE_SETTINGS);
								startActivity(callGPSSettingIntent);
							}
						});
		alertDialogBuilder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		AlertDialog alert = alertDialogBuilder.create();
		alert.show();
	}
	@Override
	public void onDestroy() {
		super.onDestroy();
		saveLocation();
		Runtime.getRuntime().gc();
	}

	private Point getCurrentLocationAsTreeLocation(){
		if (addSquatterLocationLayer != null) {
			if (addSquatterLocationLayer.getNumberOfGraphics() > 0) {
				//get current location
				//newLocationGraphic1 = new Graphic(currPoint, sym);
				int[] graphicIdArr = addSquatterLocationLayer.getGraphicIDs();
				int graphicId = graphicIdArr[0];
				Point p = (Point) addSquatterLocationLayer.getGraphic(graphicId).getGeometry();
				//map.centerAt(map.getLocationDisplayManager().getLocation().getLongitude(), map.getLocationDisplayManager().getLocation().getLatitude(), true);
				return p;
			}
		}
		return null;
	}

	private void loadTreeLocation(){
		NewSqtrLoc newSqtrLoc = DBController.loadNewSqtrLoc(SQUATTERID);
		if (newSqtrLoc==null){
			return;
		}
		Point p = new Point(newSqtrLoc.getEASTING(), newSqtrLoc.getNORTHING());
		addTreeLocation(p);
	}

	private void removeTreeLocation(){
		DBController.removeNewSqtrLoc(SQUATTERID);
		if (addSquatterLocationLayer !=null){
			addSquatterLocationLayer.removeAll();
		}
	}

	private void addTreeLocation(Point selected){

		final SimpleMarkerSymbol sym =  new SimpleMarkerSymbol(Color.RED,12, SimpleMarkerSymbol.STYLE.X);
		Point currPoint = selected;
		if (currPoint==null) {
			currPoint = map.getCenter();
			map.centerAt(currPoint, false);
		}
		if (map.getScale() > 2000) {
			map.setScale(500, false);
		}

		if (addSquatterLocationLayer == null){
			addSquatterLocationLayer = new GraphicsLayer();
			map.addLayer(addSquatterLocationLayer);
		} else {
			addSquatterLocationLayer.removeAll();
		}
		newLocationGraphic1 = new Graphic(currPoint, sym);
		addSquatterLocationLayer.addGraphic(newLocationGraphic1);

	}

	private void clearTreeLocation(){
		try {
			addSquatterLocationLayer.removeAll();
		} catch (Exception e){

		}
	}




}


