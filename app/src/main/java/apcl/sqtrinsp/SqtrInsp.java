package apcl.sqtrinsp;

import android.app.Application;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;

import java.io.File;

/**
 * Created by ronniema on 27-02-18.
 */

public class SqtrInsp extends Application {
    protected static SqtrInsp instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static Resources getResourcesApp() {
        return instance.getResources();
    }

    public static AssetManager getAssetsApp() {
        return instance.getAssets();
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

}
