package apcl.sqtrinsp.data;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by ronniema on 01-03-18.
 */

public class FileObject {
    public FileObject() {
    }

    public static void addDirToArchive(ZipOutputStream zos, File srcFile) {

        File[] files = srcFile.listFiles();

        System.out.println("Adding directory: " + srcFile.getName());

        for (int i = 0; i < files.length; i++) {

            // if the file is directory, use recursion
            if (files[i].isDirectory()) {
                addDirToArchive(zos, files[i]);
                continue;
            }

            try {

                System.out.println("tAdding file: " + files[i].getName());

                // create byte buffer
                byte[] buffer = new byte[1024];

                FileInputStream fis = new FileInputStream(files[i]);

                zos.putNextEntry(new ZipEntry(files[i].getName()));

                int length;

                while ((length = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, length);
                }

                zos.closeEntry();

                // close the InputStream
                fis.close();

            } catch (IOException ioe) {
                System.out.println("IOException :" + ioe);
            }

        }

    }


    public static Boolean write(String fpath, String fcontent){
        try {
            //String fpath = "/sdcard/"+fname+".txt";
            File file = new File(fpath);
            // If file does not exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(fcontent);
            bw.close();
            Log.d("Suceess","Sucess");
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    public static String read(String fpath){
        BufferedReader br = null;
        String response = null;
        try {
            StringBuffer output = new StringBuffer();
            //String fpath = "/sdcard/"+fname+".txt";
            br = new BufferedReader(new FileReader(fpath));
            String line = "";
            while ((line = br.readLine()) != null) {
                output.append(line +"\n");
            }
            response = output.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return response;
    }
    public static void getListInFolderNum(String directoryName, ArrayList<Integer> files,boolean recursive,String filenameSearch)
    {
        try
        {
            File directory = new File(directoryName);

            // get all the files from a directory
            File[] fList = directory.listFiles();
            Arrays.sort(fList);
            for (File file : fList) {
                try {
                    if (file.isFile()) {
                        Pattern p = Pattern.compile(filenameSearch);
                        Matcher m = p.matcher(file.getName());
                        if (m.find()) {
                            files.add(Integer.parseInt(file.getName().substring(0, file.getName().lastIndexOf("."))));
                        }
                        //if (file.getName().contains(filenameSearch))
                        //    files.add(file);
                    } else if (file.isDirectory() && recursive) {
                        getListInFolderNum(file.getAbsolutePath(), files, recursive, filenameSearch);
                    }
                }catch ( NumberFormatException e){
                    e.toString();
                }
            }


        }catch (Exception e){
            e.toString();
        }

    }
    public static void getListInFolder(String directoryName, ArrayList<File> files, boolean recursive, String filenameSearch)
    {
        try
        {
            File directory = new File(directoryName);

            // get all the files from a directory
            File[] fList = directory.listFiles();
            Arrays.sort(fList);
            for (File file : fList) {
                if (file.isFile()) {
                    Pattern p = Pattern.compile(filenameSearch);
                    Matcher m = p.matcher(file.getName());
                    if (m.find()) {
                        files.add(file);
                    }
                    //if (file.getName().contains(filenameSearch))
                    //    files.add(file);
                } else if (file.isDirectory() && recursive) {
                    getListInFolder(file.getAbsolutePath(), files, recursive,filenameSearch);
                }
            }


        }catch (Exception e){

        }

    }

    public static void DeleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                DeleteRecursive(child);

        fileOrDirectory.delete();
    }


}