package apcl.sqtrinsp.data;

import android.content.ContentValues;
import android.database.Cursor;

import com.esri.core.geometry.Point;

import java.util.ArrayList;
import java.util.List;

import apcl.sqtrinsp.SqtrInsp;
import apcl.sqtrinsp.data.dataclasses.NewSqtrLoc;
import apcl.sqtrinsp.data.dataclasses.SquatterRecord;

/**
 * Created by ronniema on 27-02-18.
 */

public class DBController {

    public static long saveNewSqtrLoc(NewSqtrLoc newSqtrLoc, String whereClause){
        ContentValues cv = new ContentValues();
        DBManager dbManager = new DBManager(SqtrInsp.getContext());
        cv.put("EASTING",newSqtrLoc.getEASTING());
        cv.put("NORTHING",newSqtrLoc.getNORTHING());
        cv.put("SQUATTERID",newSqtrLoc.getSquatterId());
        cv.put("SQUATTERLOCRMKTEXT",newSqtrLoc.getSQUATTERLOCRMKTEXT());
        cv.put("cruddate",newSqtrLoc.getCruddate());
        cv.put("crudstatus",newSqtrLoc.getCrudstatus());
        cv.put("cruduser",newSqtrLoc.getCruduser());
        if (whereClause!=null){
            dbManager.update("NEWSQTRLOC", whereClause, cv);
            return Long.valueOf(newSqtrLoc.getOBJECTID());
        } else if (newSqtrLoc.getOBJECTID()!=null){
            cv.put("OBJECTID",newSqtrLoc.getOBJECTID());
            dbManager.insert("NEWSQTRLOC", cv);
            return Long.valueOf(newSqtrLoc.getOBJECTID());
        } else {
            return dbManager.insertWithoutKey("NEWSQTRLOC", cv);
        }
    }

    public static long saveNewSqtrLoc(NewSqtrLoc newSqtrLoc){
        return saveNewSqtrLoc(newSqtrLoc, null);
    }

    public static void removeNewSqtrLoc(String SQUATTERID){
        DBManager dbManager = new DBManager(SqtrInsp.getContext());
        dbManager.executeSQL("delete from NEWSQTRLOC where SQUATTERID = '" + SQUATTERID + "'");
    }

    public static NewSqtrLoc loadNewSqtrLoc(String SQUATTERID){
        DBManager dbManager = new DBManager(SqtrInsp.getContext());
        Cursor c = dbManager.queryData("select OBJECTID, EASTING, NORTHING, SQUATTERID, SQUATTERLOCRMKTEXT, cruddate, crudstatus, cruduser from NEWSQTRLOC where SQUATTERID = '" + SQUATTERID + "'");
        try {
            if (c!=null){
                c.moveToFirst();
                if (!c.isAfterLast()) {
                    return new NewSqtrLoc(
                            c.getInt(c.getColumnIndex("OBJETID")),
                            c.getString(c.getColumnIndex("SQUATTERID")),
                            c.getString(c.getColumnIndex("SQUATTERLOCRMKTEXT")),
                            c.getDouble(c.getColumnIndex("EASTING")),
                            c.getDouble(c.getColumnIndex("NORTHING")),
                            c.getString(c.getColumnIndex("cruduser")),
                            c.getString(c.getColumnIndex("crudstatus")),
                            c.getString(c.getColumnIndex("cruddate"))
                    );
                }

            }
        } finally {
            c.close();
        }
        return null;
    }

    public static String getNewSqtrId(String parameter){
        DBManager dbManager = new DBManager(SqtrInsp.getContext());
        String tempNumber = dbManager.getData("select ifnull(substr(squatterid, instr(squatterid, 'HAHQ-T') + 6 ), 0) as indexNumber from record " +
                " where squatterid like '%" + parameter + "%'" +
                " order by squatterid desc limit 1");
        return parameter + String.format("%04d", Integer.parseInt(tempNumber == "" ? "0" : tempNumber) + 1);
    }

    public static String getNewSqtrId(){
        return getNewSqtrId("HAHQ-T");
    }

    public static long saveSqtrRecord(SquatterRecord squatterRecord, String whereClause){
        ContentValues cv = new ContentValues();
        DBManager dbManager = new DBManager(SqtrInsp.getContext());

        cv.put("squatterid",squatterRecord.getSquatterid());
        cv.put("objectid",squatterRecord.getObjectid());
        cv.put("bookno",squatterRecord.getBookno());
        cv.put("filename",squatterRecord.getFilename());
        cv.put("fileref",squatterRecord.getFileref());
        cv.put("dlooffice",squatterRecord.getDlooffice());
        cv.put("dso",squatterRecord.getDso());

        cv.put("footdimension_b",squatterRecord.getFootdimension_b());
        cv.put("footdimension_h",squatterRecord.getFootdimension_h());
        cv.put("footdimension_l",squatterRecord.getFootdimension_l());
        cv.put("hasremark",squatterRecord.getHasremark());
        cv.put("houseno",squatterRecord.getHouseno());
        cv.put("issue",squatterRecord.getIssue());
        cv.put("location",squatterRecord.getLocation());

        cv.put("materials",squatterRecord.getMaterials());
        cv.put("planfilename",squatterRecord.getPlanfilename());
        cv.put("recorddate",squatterRecord.getRecorddateStr());
        cv.put("scoffice",squatterRecord.getScoffice());
        cv.put("serialno",squatterRecord.getSerialno());
        cv.put("status",squatterRecord.getStatus());
        cv.put("surveyno",squatterRecord.getSurveyno());

        cv.put("surveynoprefix",squatterRecord.getSurveynoprefix());
        cv.put("usage",squatterRecord.getUsage());
        cv.put("cruduser",squatterRecord.getCruddate());
        cv.put("crudstatus",squatterRecord.getCrudstatus());
        cv.put("cruddate",squatterRecord.getCruduser());

        if (whereClause!=null){
            dbManager.update("RECORD", whereClause, cv);
            return Long.valueOf(squatterRecord.getObjectid());
        } else if (squatterRecord.getObjectid()!=null){
            cv.put("OBJECTID",squatterRecord.getObjectid());
            dbManager.insert("RECORD", cv);
            return Long.valueOf(squatterRecord.getObjectid());
        } else {
            return dbManager.insertWithoutKey("RECORD", cv);
        }
    }

    public static long saveSqtrRecord(SquatterRecord squatterRecord){
        return saveSqtrRecord(squatterRecord, null);
    }

    public static void removeSqtrRecord(String SQUATTERID){
        DBManager dbManager = new DBManager(SqtrInsp.getContext());
        dbManager.executeSQL("delete from RECORD where SQUATTERID = '" + SQUATTERID + "'");
        removeNewSqtrLoc(SQUATTERID);
    }

    public static SquatterRecord loadSqtrRecord(Cursor c){
        return new SquatterRecord(
                c.getString(c.getColumnIndex("squatterid")),
                c.getInt(c.getColumnIndex("objectid")),
                c.getString(c.getColumnIndex("bookno")),
                c.getString(c.getColumnIndex("filename")),
                c.getString(c.getColumnIndex("fileref")),
                c.getInt(c.getColumnIndex("dlooffice")),
                c.getInt(c.getColumnIndex("dso")),
                c.getInt(c.getColumnIndex("footdimension_b")),
                c.getInt(c.getColumnIndex("footdimension_h")),
                c.getInt(c.getColumnIndex("footdimension_l")),
                c.getString(c.getColumnIndex("hasremark")),
                c.getString(c.getColumnIndex("houseno")),
                c.getString(c.getColumnIndex("issue")),
                c.getString(c.getColumnIndex("location")),
                c.getInt(c.getColumnIndex("materials")),
                c.getString(c.getColumnIndex("planfilename")),
                c.getString(c.getColumnIndex("recorddate")),
                c.getInt(c.getColumnIndex("scoffice")),
                c.getString(c.getColumnIndex("serialno")),
                c.getInt(c.getColumnIndex("status")),
                c.getString(c.getColumnIndex("surveyno")),
                c.getString(c.getColumnIndex("surveynoprefix")),
                c.getInt(c.getColumnIndex("usage")),
                c.getString(c.getColumnIndex("cruduser")),
                c.getString(c.getColumnIndex("crudstatus")),
                c.getString(c.getColumnIndex("cruddate"))
        );
    }

    public static SquatterRecord loadSqtrRecord(String SQUATTERID){
        DBManager dbManager = new DBManager(SqtrInsp.getContext());
        Cursor c = dbManager.queryData("select squatterid, objectid, bookno, filename, fileref, dlooffice, dso, " +
                " footdimension_b, footdimension_h, footdimension_l, hasremark, houseno, issue, location," +
                " materials, planfilename, recorddate, scoffice, serialno, status, surveyno," +
                " surveynoprefix, usage, cruduser, crudstatus, cruddate " +
                " from RECORD where SQUATTERID = '" + SQUATTERID + "'");
        if (c!=null){
            try {
                c.moveToFirst();
                if (!c.isAfterLast()) {
                    return loadSqtrRecord(c);
                }
            } finally {
                c.close();
            }
        }
        return null;
    }

    public static List<SquatterRecord> loadSqtrRecordList(){
        List<SquatterRecord> arrList = new ArrayList<>();
        DBManager dbManager = new DBManager(SqtrInsp.getContext());
        Cursor c = dbManager.queryData("select squatterid, objectid, bookno, filename, fileref, dlooffice, dso, " +
                " footdimension_b, footdimension_h, footdimension_l, hasremark, houseno, issue, location," +
                " materials, planfilename, recorddate, scoffice, serialno, status, surveyno," +
                " surveynoprefix, usage, cruduser, crudstatus, cruddate " +
                " from RECORD where crudstatus <> 'D' ");
        try {
            if (c!=null){

                c.moveToFirst();
                while (!c.isAfterLast()) {
                    arrList.add(loadSqtrRecord(c));
                    c.moveToNext();
                }
            }
        } finally {
            c.close();
        }
        return arrList;
    }

}
