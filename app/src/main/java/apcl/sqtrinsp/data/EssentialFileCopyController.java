package apcl.sqtrinsp.data;

import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import apcl.sqtrinsp.R;
import apcl.sqtrinsp.SqtrInsp;

/**
 * Created by ronniema on 27-02-18.
 */

public class EssentialFileCopyController {

    public static void fileCheck(String FILE_PATH, String FILE_NAME, String ASSET_SOURCE_NAME){
        File file = new File(FILE_PATH + File.separator + FILE_NAME);
        if (!file.exists()){
            copySourceFile(FILE_PATH, FILE_NAME, ASSET_SOURCE_NAME);
        }
    }

    public static void fileCheck(String type, Boolean delete){
        String FILE_PATH = SqtrInsp.getContext().getFilesDir().toString() + SqtrInsp.getResourcesApp().getString(R.string.database_path);
        String FILE_NAME = SqtrInsp.getResourcesApp().getString(R.string.database_name);
        String ASSET_SRC_NAME = SqtrInsp.getResourcesApp().getString(R.string.database_name);
        if (type.equals("map")){
            FILE_PATH = SqtrInsp.getContext().getFilesDir().toString() + SqtrInsp.getResourcesApp().getString(R.string.map_path);
            FILE_NAME = SqtrInsp.getResourcesApp().getString(R.string.map_name);
            ASSET_SRC_NAME = SqtrInsp.getResourcesApp().getString(R.string.map_name);
        } else if (type.equals("map2")){
            FILE_PATH = SqtrInsp.getContext().getFilesDir().toString() + SqtrInsp.getResourcesApp().getString(R.string.map_path);
            FILE_NAME = SqtrInsp.getResourcesApp().getString(R.string.map2_name);
            ASSET_SRC_NAME = SqtrInsp.getResourcesApp().getString(R.string.map2_name);
        }

        if (delete){
            deleteDatabase(FILE_PATH, FILE_NAME);
        }
        fileCheck(FILE_PATH, FILE_NAME, ASSET_SRC_NAME);
    }

    public static void deleteDatabase(String DATABASE_PATH, String DATABASE_NAME){
        File file = new File(DATABASE_PATH + File.separator + DATABASE_NAME);
        boolean deleted = file.delete();
    }

    public static void copySourceFile(String newPath, String databaseName, String assetSourceName) {
        byte[] buffer = new byte[1024];
        OutputStream myOutput = null;
        int length;
        InputStream myInput = null;
        try {
            File f = new File(newPath);
            if (!f.isDirectory()) {
                f.mkdirs();
            }
            myInput = SqtrInsp.getAssetsApp().open(assetSourceName);
            myOutput =new FileOutputStream(newPath + File.separator + databaseName);
            while((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }
            myOutput.close();
            myOutput.flush();
            myInput.close();

            Log.i("Database", "db created");
        } catch(IOException e) {
            Log.i("Database", "db cannot be created");
            e.printStackTrace();
        }
    }

}
