package apcl.sqtrinsp.data.dataclasses;
import java.text.ParseException;
import java.util.Date;

import apcl.sqtrinsp.CommonFunction;
import apcl.sqtrinsp.data.DBController;

/**
 * Created by ronniema on 26-02-18.
 */

public class SquatterRecord extends CrudAttributes {

    /*@SerializedName("user_name")
    @Expose
    String userId;*/
    private Integer objectid;
    private String squatterid;

    private String bookno, filename, fileref;
    private Integer dlooffice, dso;
    private Integer footdimension_b, footdimension_h, footdimension_l;
    private String hasremark, houseno, issue;
    private String location;
    private Integer materials;

    private String planfilename;
    private Date recorddate;
    private Integer scoffice;
    private String serialno;

    private Integer status;
    private String surveyno;
    private String surveynoprefix;
    private Integer usage;

    public SquatterRecord (
            String squatterid,
            Integer objectid,
            String bookno,
            String filename,
            String fileref,
            Integer dlooffice,
            Integer dso,
            Integer footdimension_b,
            Integer footdimension_h,
            Integer footdimension_l,
            String hasremark,
            String houseno,
            String issue,
            String location,
            Integer materials,
            String planfilename,
            String recorddate,
            Integer scoffice,
            String serialno,
            Integer status,
            String surveyno,
            String surveynoprefix,
            Integer usage,
            String cruduser,
            String crudstatus,
            String cruddate
    ){
        this.squatterid = squatterid;
        this.objectid = objectid;
        this.bookno = bookno;
        this.filename = filename;
        this.fileref = fileref;
        this.dlooffice = dlooffice;
        this.dso = dso;
        this.footdimension_b = footdimension_b;
        this.footdimension_h = footdimension_h;
        this.footdimension_l = footdimension_l;
        this.hasremark = hasremark;
        this.houseno = houseno;
        this.issue = issue;
        this.location = location;
        this.materials = materials;
        this.planfilename = planfilename;
        try {
            this.recorddate = CommonFunction.stringConvertIntoDate(recorddate);
        } catch (ParseException e){
            this.recorddate = CommonFunction.getSystemDate();
        }
        this.scoffice = scoffice;
        this.serialno = serialno;
        this.status = status;
        this.surveyno = surveyno;
        this.surveynoprefix = surveynoprefix;
        this.usage = usage;
        this.cruduser = cruduser;
        this.crudstatus = crudstatus;
        this.cruddate = cruddate;
    }

    public SquatterRecord (
            Boolean dummy
    ){
        if (dummy) {
            this.squatterid = DBController.getNewSqtrId();
            this.objectid = null;
            this.bookno = "Bkn Num";
            this.filename = "FRefSerialNo1";
            this.fileref = "File Ref";
            this.dlooffice = 1;
            this.dso = 1;
            this.footdimension_b = 0;
            this.footdimension_h = 0;
            this.footdimension_l = 0;
            this.hasremark = "has remark";
            this.houseno = "Hse Num 1";
            this.issue = "issue 1";
            this.location = "location 1";
            this.materials = 1;
            this.planfilename = "Survey Sheet No.";
            this.recorddate = CommonFunction.getSystemDate();
            this.scoffice = 1;
            this.serialno = "Serial No 1";
            this.status = 1;
            this.surveyno = "Survey No 1";
            this.surveynoprefix = "Srvy";
            this.usage = 1;
        } else {
            this.squatterid = DBController.getNewSqtrId();
            this.objectid = -1;
            this.bookno = "";
            this.filename = "";
            this.fileref = "";
            this.dlooffice = 1;
            this.dso = 1;
            this.footdimension_b = 0;
            this.footdimension_h = 0;
            this.footdimension_l = 0;
            this.hasremark = "";
            this.houseno = "";
            this.issue = "";
            this.location = "";
            this.materials = 1;
            this.planfilename = "";
            this.recorddate = CommonFunction.getSystemDate();
            this.scoffice = 1;
            this.serialno = "";
            this.status = 1;
            this.surveyno = "";
            this.surveynoprefix = "";
            this.usage = 1;
        }
    }

    public String getBookno() {
        return bookno;
    }

    public void setBookno(String bookno) {
        this.bookno = bookno;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFileref() {
        return fileref;
    }

    public void setFileref(String fileref) {
        this.fileref = fileref;
    }

    public Integer getDlooffice() {
        return dlooffice;
    }

    public void setDlooffice(Integer dlooffice) {
        this.dlooffice = dlooffice;
    }

    public Integer getDso() {
        return dso;
    }

    public void setDso(Integer dso) {
        this.dso = dso;
    }

    public Integer getFootdimension_b() {
        return footdimension_b;
    }

    public void setFootdimension_b(Integer footdimension_b) {
        this.footdimension_b = footdimension_b;
    }

    public Integer getFootdimension_h() {
        return footdimension_h;
    }

    public void setFootdimension_h(Integer footdimension_h) {
        this.footdimension_h = footdimension_h;
    }

    public Integer getFootdimension_l() {
        return footdimension_l;
    }

    public void setFootdimension_l(Integer footdimension_l) {
        this.footdimension_l = footdimension_l;
    }

    public String getHasremark() {
        return hasremark;
    }

    public void setHasremark(String hasremark) {
        this.hasremark = hasremark;
    }

    public String getHouseno() {
        return houseno;
    }

    public void setHouseno(String houseno) {
        this.houseno = houseno;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getMaterials() {
        return materials;
    }

    public void setMaterials(Integer materials) {
        this.materials = materials;
    }

    public Integer getObjectid() {
        return objectid;
    }

    public void setObjectid(Integer objectid) {
        this.objectid = objectid;
    }

    public String getPlanfilename() {
        return planfilename;
    }

    public void setPlanfilename(String planfilename) {
        this.planfilename = planfilename;
    }

    public Date getRecorddate() {
        return recorddate;
    }

    public String getRecorddateStr() {
        return CommonFunction.dateConvertIntoString(recorddate);
    }

    public void setRecorddate(Date recorddate) {
        this.recorddate = recorddate;
    }

    public Integer getScoffice() {
        return scoffice;
    }

    public void setScoffice(Integer scoffice) {
        this.scoffice = scoffice;
    }

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public String getSquatterid() {
        return squatterid;
    }

    public void setSquatterid(String squatterid) {
        this.squatterid = squatterid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getSurveyno() {
        return surveyno;
    }

    public void setSurveyno(String surveyno) {
        this.surveyno = surveyno;
    }

    public String getSurveynoprefix() {
        return surveynoprefix;
    }

    public void setSurveynoprefix(String surveynoprefix) {
        this.surveynoprefix = surveynoprefix;
    }

    public Integer getUsage() {
        return usage;
    }

    public void setUsage(Integer usage) {
        this.usage = usage;
    }
}
