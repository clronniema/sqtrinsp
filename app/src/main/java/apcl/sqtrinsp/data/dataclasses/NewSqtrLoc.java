package apcl.sqtrinsp.data.dataclasses;

/**
 * Created by ronniema on 27-02-18.
 */

public class NewSqtrLoc extends CrudAttributes {

    Integer OBJECTID;
    String SquatterId;
    String SQUATTERLOCRMKTEXT;
    Double EASTING, NORTHING;

    public NewSqtrLoc (
            Integer OBJECTID,
            String SquatterId,
            String SQUATTERLOCRMKTEXT,
            Double EASTING,
            Double NORTHING
    ){
        this.OBJECTID = OBJECTID;
        this.SquatterId = SquatterId;
        this.SQUATTERLOCRMKTEXT = SQUATTERLOCRMKTEXT;
        this.EASTING = EASTING;
        this.NORTHING = NORTHING;
    }

    public NewSqtrLoc (
            Integer OBJECTID,
            String SquatterId,
            String SQUATTERLOCRMKTEXT,
            Double EASTING,
            Double NORTHING,
            String cruduser,
            String crudstatus,
            String cruddate
    ){
        this.OBJECTID = OBJECTID;
        this.SquatterId = SquatterId;
        this.SQUATTERLOCRMKTEXT = SQUATTERLOCRMKTEXT;
        this.EASTING = EASTING;
        this.NORTHING = NORTHING;
        this.cruddate = cruddate;
        this.crudstatus = crudstatus;
        this.cruduser = cruduser;
    }

    public Integer getOBJECTID() {
        return OBJECTID;
    }

    public void setOBJECTID(Integer OBJECTID) {
        this.OBJECTID = OBJECTID;
    }

    public String getSquatterId() {
        return SquatterId;
    }

    public void setSquatterId(String squatterId) {
        this.SquatterId = squatterId;
    }

    public String getSQUATTERLOCRMKTEXT() {
        return SQUATTERLOCRMKTEXT;
    }

    public void setSQUATTERLOCRMKTEXT(String SQUATTERLOCRMKTEXT) {
        this.SQUATTERLOCRMKTEXT = SQUATTERLOCRMKTEXT;
    }

    public Double getEASTING() {
        return EASTING;
    }

    public void setEASTING(Double EASTING) {
        this.EASTING = EASTING;
    }

    public Double getNORTHING() {
        return NORTHING;
    }

    public void setNORTHING(Double NORTHING) {
        this.NORTHING = NORTHING;
    }
}
