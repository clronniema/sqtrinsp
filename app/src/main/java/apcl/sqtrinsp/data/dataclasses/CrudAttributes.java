package apcl.sqtrinsp.data.dataclasses;

import java.sql.Date;

import apcl.sqtrinsp.CommonFunction;

/**
 * Created by ronniema on 26-02-18.
 */

class CrudAttributes {
    String cruduser;
    String crudstatus;
    String cruddate;

    public CrudAttributes() {
        this.cruduser = "apcl_test";
        this.crudstatus = "U";
        this.cruddate = CommonFunction.getSystemDateString();
    }

    public CrudAttributes(String crudstatus) {
        this.cruduser = "apcl_test";
        this.crudstatus = crudstatus;
        this.cruddate = CommonFunction.getSystemDateString();
    }


    public void setCruduser(String cruduser) {
        this.cruduser = cruduser;
    }

    public void setCrudstatus(String crudstatus) {
        this.crudstatus = crudstatus;
    }

    public void setCruddate(String cruddate) {
        this.cruddate = cruddate;
    }

    public void setCruddate(Date cruddate) {
        this.cruddate = CommonFunction.dateConvertIntoString(cruddate);
    }

    public String getCruduser() {
        return cruduser;
    }

    public String getCrudstatus() {
        return crudstatus;
    }

    public String getCruddate() {
        return cruddate;
    }
}
