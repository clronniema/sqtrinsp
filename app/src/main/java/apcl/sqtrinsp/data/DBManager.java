
package apcl.sqtrinsp.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Environment;
import android.util.Log;


import net.sqlcipher.database.SQLiteConstraintException;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.util.HashMap;
import java.util.List;

import apcl.sqtrinsp.R;
import apcl.sqtrinsp.SqtrInsp;


public class DBManager {

    public String DATABASE_PATH = SqtrInsp.getContext().getFilesDir() + File.separator + SqtrInsp.getResourcesApp().getString(R.string.database_path);
    public String DATABASE_NAME = SqtrInsp.getResourcesApp().getString(R.string.database_name);
    private Context c;
    public static SQLiteDatabase db;

    public DBManager(Context c){
        this.c = c;
        //c.openOrCreateDatabase(DATABASE_NAME, c.MODE_PRIVATE, null);
    }

    public synchronized Boolean databaseExist(){
        File file = new File(DATABASE_PATH+DATABASE_NAME);
        return file.exists();
    }

    public synchronized Boolean executeSQL(String sql) {
        SQLiteDatabase.loadLibs(c);
        Boolean result = false;
        SQLiteDatabase db = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME,c.getResources().getString(R.string.encryption),null,0);
        try {
            //db = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME, null, SQLiteDatabase.OPEN_READWRITE);
            db.execSQL(sql);
            result = true;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            result = false;
        } finally {
            db.close();
        }
        return result;
    }

    public synchronized Boolean insert(String tablename, ContentValues cv) {
       SQLiteDatabase.loadLibs(c);
        try {
            SQLiteDatabase db = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME,c.getResources().getString(R.string.encryption),null,0);
            //db = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME, null, SQLiteDatabase.OPEN_READWRITE);
            db.insert(tablename, null, cv);
            db.close();
            return true;
        }
        catch (Exception e1){
            e1.printStackTrace();
            return false;
        }
    }

    public synchronized long insertWithoutKey(String tablename, ContentValues cv) {
        SQLiteDatabase.loadLibs(c);
        try {
            SQLiteDatabase db = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME,c.getResources().getString(R.string.encryption),null,0);
            //db = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME, null, SQLiteDatabase.OPEN_READWRITE);
            long id = db.insert(tablename, null, cv);
            db.close();
            return id;
        }
        catch (Exception e1){
            e1.printStackTrace();
            return 0;
        }
    }

    public synchronized Boolean insert(String tablename, List<ContentValues> cv)
    {
        SQLiteDatabase.loadLibs(c);
        try {
            SQLiteDatabase db = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME,c.getResources().getString(R.string.encryption),null,0);
            //db = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME, null, SQLiteDatabase.OPEN_READWRITE);
            db.beginTransaction();
            //ResultSet rs = QueryData(l);
            //db.execSQL(sql);
            if (cv!=null && cv.size()>0)
            {
                for (int i=0;i<cv.size();i++)
                {
                    db.insert(tablename, null, cv.get(i));
                }
            }
            db.setTransactionSuccessful();
            db.endTransaction();
            db.close();
            //Cursor allrows  = db.rawQuery("SELECT * FROM test,test1 ", null);

            return true;
        } catch (SQLiteConstraintException e) {
            //Toast.makeText(thi, "No", Toast.LENGTH_LONG).show();
            Log.d("*** Error ***",e.getMessage());

            System.out.print(e.getMessage());
            return false;
        }
    }

    /**
     * update sqlite table with where string containing key e.g. "id=1"
     */
    public synchronized Boolean update(String tablename, String where_key, ContentValues cv) {
        SQLiteDatabase.loadLibs(c);
        try {
            SQLiteDatabase db = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME,c.getResources().getString(R.string.encryption),null,0);
            //db = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME, null, SQLiteDatabase.OPEN_READWRITE);
            db.update(tablename, cv, where_key, null);
            return true;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return false;
        }

    }

    public synchronized Boolean update(String tablename, String where_key, HashMap<String, ContentValues> lst_cv) {
        SQLiteDatabase.loadLibs(c);
        for (String key : lst_cv.keySet()) {
            if (where_key.equals("case_no")) {
                try {
                    SQLiteDatabase db = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME, c.getResources().getString(R.string.encryption), null, 0);
                    //db = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME, null, SQLiteDatabase.OPEN_READWRITE);
                    db.update(tablename, lst_cv.get(key), "case_no = '" + key + "'", null);
                    //return true;
                } catch (Exception e) {
                    System.out.print(e.getMessage());
                    return false;
                }
            }
        }
        return true;
    }



    public synchronized String getData(String sql) {
        SQLiteDatabase.loadLibs(c);
        Cursor allrows = null;
        String temp = "";
        SQLiteDatabase db = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME,c.getResources().getString(R.string.encryption),null,0);
        try {
            //db = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME, null, SQLiteDatabase.OPEN_READWRITE);
            allrows = db.rawQuery(sql, null);
            allrows.moveToFirst();
            temp = allrows.getString(0);

        } catch (Exception e) {
            System.out.print(e.getMessage());
        } finally {
            if (allrows!= null) {
                allrows.close();
                db.close();
            }
        }
        return temp;
    }

    public synchronized net.sqlcipher.Cursor queryData(String sql) {
        SQLiteDatabase.loadLibs(c);
        net.sqlcipher.Cursor allrows = null;
        try {
            SQLiteDatabase db = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME,c.getResources().getString(R.string.encryption),null,0);
            //db = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME, null, SQLiteDatabase.OPEN_READWRITE);
            allrows = db.rawQuery(sql, null);
        } catch (Exception e) {
            System.out.print(e.getMessage());
            Log.d("dev log", e.toString());
        }
        return allrows;
    }

    public synchronized void close(){
        if (db!=null) {
            if (db.isOpen()) {
                db.close();
            }
        }
    }
}




